package org.kazminov.mycardlistdemo.utils;

import android.util.Log;

/**
 * Created by olkazmin on 01.05.16.
 */
public class Logger {
    public static void v(String tag, String message, Object... args) {
        Log.v(tag, String.format(message, args));
    }
}
