package org.kazminov.mycardlistdemo;

/**
 * Created by oleg on 18/04/16.
 */
public class BackField {
    private final String mTextAlignment;
    private final String mKey;
    private final String mValue;
    private final String mLabel;

    public BackField(String key, String value, String label, String textAlignment) {
        mKey = key;
        mValue = value;
        mLabel = label;
        mTextAlignment = textAlignment;
    }

    public String getLabel() {
        return mLabel;
    }

    public String getValue() {
        return mValue;
    }

    @Override
    public String toString() {
        return String.format("------------\nLabel: %s \nValue: %s\n------------\n", mLabel, mValue);
    }
}
