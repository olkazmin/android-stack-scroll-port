package org.kazminov.mycardlistdemo;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.android.systemui.statusbar.stack.NotificationStackScrollLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Object> {

    public static final String LOG_TAG = Constants.LOG_TAG_PREFFIX + "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<Object>(this) {

            @Override
            protected void onStartLoading() {
                forceLoad();
            }

            @Override
            public Object loadInBackground() {
                final ArrayList<BackField> result = new ArrayList<>();
                try {

                    final InputStream fileStream = getAssets().open("pass.json");
                    final ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    final byte[] buffer = new byte[1024];
                    int length = 0;

                    while((length = fileStream.read(buffer)) != -1) {
                        bos.write(buffer, 0, length);
                    }

                    final byte[] bytes = bos.toByteArray();
                    final String strJson = new String(bytes);
                    final JSONObject json = new JSONObject(strJson);

                    final JSONObject storeCard = json.getJSONObject("storeCard");
                    final JSONArray backFields = storeCard.getJSONArray("backFields");
                    for (int i = 0; i <backFields.length(); i++) {
                        final JSONObject backField = backFields.getJSONObject(i);
                        if (backField != null) {
                            final String key = backField.getString("key");
                            final String value = backField.getString("value");
                            final String label = backField.getString("label");

                            final String textAlignment = backField.has("textAlignment") ? backField.getString("textAlignment") : "";

                            result.add(new BackField(key, value, label, textAlignment));
                        }
                    }

                } catch (IOException e) {
                    throw new Error(e);
                } catch (JSONException e) {
                    throw new Error(e);
                }

                return result;
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        if (data instanceof ArrayList) {
            final ArrayList<BackField> backFields = (ArrayList<BackField>) data;
            handleBackFields(backFields);
        }
    }

    private void handleBackFields(ArrayList<BackField> backFields) {
        Log.v(LOG_TAG, "handleBackFields: items count=" + backFields.size());
        final NotificationStackScrollLayout stackScrollView = (NotificationStackScrollLayout) findViewById(R.id.stack_scroll_layout);
        final LayoutInflater inflater = LayoutInflater.from(this);
        for (int i = 0; i < backFields.size();i++) {
            final BackField backField = backFields.get(i);
            final View view = inflater.inflate(R.layout.stack_back_field_item, null);
            final TextView labelView = (TextView) view.findViewById(R.id.label_view);
            final TextView valueView = (TextView) view.findViewById(R.id.value_view);

            if (!TextUtils.isEmpty(backField.getLabel())) {
                labelView.setText(backField.getLabel());
            }

            valueView.setText(backField.getValue());
            view.setTag(Integer.valueOf(i));


            stackScrollView.addView(view);
        }

        stackScrollView.setScrollingEnabled(true);
        stackScrollView.setAnimationsEnabled(true);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }
}
