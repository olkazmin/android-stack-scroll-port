package org.kazminov.mycardlistdemo;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;

import com.android.systemui.statusbar.ExpandableView;

/**
 * Created by oleg on 18/05/16.
 */
public class MyExpandableCard extends ExpandableView {

    private final Handler mHandler;

    public MyExpandableCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHandler = new Handler();
    }

    @Override
    public void performRemoveAnimation(long duration, float translationDirection, final Runnable onFinishedRunnable) {
        mHandler.postDelayed(onFinishedRunnable, 500);
    }

    @Override
    public void performAddAnimation(long delay, long duration) {
    }
}
