package org.kazminov.mycardlistdemo;

/**
 * Created by olkazmin on 01.05.16.
 */
public interface Constants {
    public static final String LOG_TAG = "MyDemo";
    public static final String LOG_TAG_PREFFIX = LOG_TAG + "::";
}
